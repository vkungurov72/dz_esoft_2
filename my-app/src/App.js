import Navbar from './Components/Navbar';
import Layout from './Components/Layout';
import Main from './Components/Main';
import './styles/App.css';

import React from 'react';

const App = () => {
  return (
    <div className="app">
      <Navbar />
      <Main />
      <Layout />
    </div>
  );
}

export default App;
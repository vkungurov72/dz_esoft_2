import React from 'react'

const CompetenceComponent = ({ competence }) => {
  const { name, description, level, skillsToLearn } = competence
  const isICan = level > 0

  return (
    <div className='competence'>
      <div
        className={isICan ? 'competence-block can' : 'competence-block cant'}
      >
        <h1 className='competence-name'>{name}</h1>
        <p className='competence-info'>{description}</p>
        {level && (
          <p className='competence-level'>
            Я оцениваю свои знания на{' '}
            <span className='competence-levelnumber'>{level}</span>%
          </p>
        )}
        {skillsToLearn && (
          <ul className='competence-skills'>
            <p>Желаемые для изучения навыки:</p>
            {skillsToLearn.map((skill, index) => (
              <li key={index}>{skill}</li>
            ))}
          </ul>
        )}
      </div>
    </div>
  )
}

export default CompetenceComponent

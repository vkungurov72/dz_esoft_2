import React from 'react'
const Navbar = () => {
  const title = 'Портфолио'

  return (
    <header className='header'>
      <h1 className='header-title'>{title}</h1>
      <nav className='header-nav'>
        <a href='https://t.me/Kuklystyle' className='header-link'>
          Мой Telegram
        </a>
      </nav>
    </header>
  )
}

export default Navbar

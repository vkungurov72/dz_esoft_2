import React from 'react'
const Layout = () => {
  return (
    <div className='layout'>
      <footer className='footer'>
        © 2024 Все права защищены. Это задание было разработано исключительно в
        учебных целях.
      </footer>
    </div>
  )
}

export default Layout

import React, { useState } from 'react'
import Photo from '../assets/myPhoto.jpg'
import MyInformation from './MyInformation'
import Competence from './Competence'

const Main = () => {
  const informationMe = {
    myImage: Photo,
    myName: 'Кунгуров Вадим Иванович',
    myDescription:
      'Хочу немного рассказать о себе, в прошлом году проходил курсы в Школе программирования, сдавал проект - результатом был только Сертификат, в этом году постараюсь выложиться на Максимум!'
  }

  const [competences, setCompetences] = useState([
    {
      name: 'HTML/CSS',
      description:
        'Я начал свой путь с банальной верстки. Попробовал html + css, верстал разные лендинги (макеты брал открытые из интернета).',
      level: 55,
      isICan: true
    },
    {
      name: 'JavaScript',
      description:
        'Уделял время JS, для того чтобы можно было свои макеты "оживлять". Изучал основные принципы ООП и понемногу углублялся в js,как он работает, какие у него особенности.',
      level: 50,
      isICan: true
    },
    {
      name: 'React',
      description:
        'В прошлом году создавал на React проект про Фильмы. Также могу сказать про работу с React Router, использование Context, получение данных с сервера и их отображение. В рамках курса моя цель - значительно улучшить свои навыки разработки на React.',
      level: 45,
      isICan: true
    },
    {
      name: 'Node.js/Express',
      description:
        'На Node.js умею создавать веб-сервера и интегрировать их с базами данных. Также изучал как Node.js работает с PostgreSQL и Express.',
      level: 45,
      isICan: true
    },
    {
      name: 'PostgreSQL',
      description:
        'Я умею создавать таблицы, выполнять запросы к базе данных, подключать её к веб-серверу и обрабатывать данные с помощью NodeJS.',
      level: 45,
      isICan: true
    },
    {
      name: 'Git',
      description:
        'С помощью Git я создавал репозитории для проектов, отслеживал состояние файлов, фиксировал изменения с коммитами, смотрел историю изменений, работал с ветками для параллельной разработки.',
      level: 55,
      isICan: true
    },
    {
      name: 'Хуки React',
      description:
        'Хуки React - отличный способ управления состоянием и жизненным циклом компонентов в React. Знаю основные хуки useState, useEffect и useContext.',
      level: 25,
      isICan: false
    },
    {
      name: 'TypeScript',
      description:
        'С TypeScript знаком очень мало. Знаю, что в нем нужно указывать типы переменных, создавать типы и интерфейсы для объектов. TypeScript делает код более строгим и безопасным, а работу с ним - настоящим удовольствием.',
      skillsToLearn: [
        'Статическая типизация',
        'Использование интерфейсов',
        'Преимущества по сравнению с JavaScript'
      ],
      level: null,
      isICan: false
    },
    {
      name: 'PostgreSQL',
      skillsToLearn: [
        'Хочу научиться делать запросы к базе данных еще качественнее!',
        'Лучше оптимизировать структуру базы данных, использовать индексы для ускорения поиска и правильно проектировать связи между таблицами.',
        'Улучшить процесс получения и отправки данных в базу.'
      ],
      level: null,
      isICan: false
    },
    {
      name: 'NodeJS/Express',
      skillsToLearn: [
        'Познакомься с концепцией middleware в Express и научиться создавать собственные middleware функции для обработки запросов.',
        'Изучить методики тестирования Node.js и Express приложений с помощью фреймворков'
      ],
      level: null,
      isICan: false
    },
    {
      name: 'Хуки React',
      description:
        'Хочу расширить свои знания и познакомиться с другими хуками для еще более эффективной разработки.',
      skillsToLearn: [
        'Изучение других хуков React',
        'Оптимизация работы с состоянием',
        'Работа с пользовательскими хуками'
      ],
      level: null,
      isICan: false
    },
    {
      name: 'Redux',
      description:
        'Redux - это популярная библиотека управления состоянием в приложениях на React. Она помогает эффективно управлять данными и состоянием приложения, делая его более предсказуемым и легко масштабируемым.',
      skillsToLearn: [
        'Глубокое понимание принципов работы Redux',
        'Умение создавать и использвать actions и reducers',
        'Интеграция Redux в приложения на React',
        'Отладка и оптимизация Redux-приложений'
      ],
      level: null,
      isICan: false
    },
    {
      name: 'Чистый и понятный код',
      description:
        'Чистый код - это важный принцип разработки программного обеспечения, который подразумевает написание читаемого, понятного и легко поддерживаемого кода. Это позволяет сделать код более эффективным, уменьшить количество ошибок и упростить его дальнейшее развитие.',
      skillsToLearn: [
        'Принципы чистого кода',
        'Инструменты для анализа и улучшения кода',
        'Методики рефакторинга кода',
        'Практические навыки написания чистого и понятного кода'
      ],
      level: null,
      isICan: false
    }
  ])

  return (
    <>
      <main className='main'>
        <MyInformation
          myImage={informationMe.myImage}
          myName={informationMe.myName}
          myDescription={informationMe.myDescription}
        />
        <section className='section'>
          <Competence
            competences={competences}
            setCompetences={setCompetences}
          />
        </section>
      </main>
    </>
  )
}

export default Main

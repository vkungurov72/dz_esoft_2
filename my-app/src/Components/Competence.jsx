import React, { useState } from 'react'
import CompetenceComponent from './CompetenceComponent'
import CompetenceButton from './CompetenceButton'
import CompetenceManagement from './CompetenceManagement'

const Competence = ({ competences, setCompetences }) => {
  const [showHighLevel, setShowHighLevel] = useState(false)
  const [showLowLevel, setShowLowLevel] = useState(false)
  const [showAll, setShowAll] = useState(false)
  const handleShowHighLevel = () => {
    setShowHighLevel(!showHighLevel)
    setShowAll(false)
  }

  const handleShowLowLevel = () => {
    setShowLowLevel(!showLowLevel)
    setShowAll(false)
  }

  const handleShowAll = () => {
    setShowAll(!showAll)
    setShowHighLevel(false)
    setShowLowLevel(false)
  }

  const filteredCompetences = competences.filter(
    competence =>
      (showHighLevel && competence.level > 50) ||
      (showLowLevel && competence.level <= 50 && competence.level !== 0) ||
      showAll
  )

  return (
    <div>
      <CompetenceButton
        text='Все компетенции'
        onClick={handleShowAll}
        isToggled={showAll}
      />
      <CompetenceButton
        text='Компетенции с уровнем изучения более 50%'
        onClick={handleShowHighLevel}
        isToggled={showHighLevel}
      />
      <CompetenceButton
        text='Компетенции с уровнем изучения менее 50%'
        onClick={handleShowLowLevel}
        isToggled={showLowLevel}
      />
      {filteredCompetences.map((competence, index) => (
        <CompetenceComponent key={index} competence={competence} />
      ))}
      <CompetenceManagement
        competences={competences}
        setCompetences={setCompetences}
      />
    </div>
  )
}

export default Competence

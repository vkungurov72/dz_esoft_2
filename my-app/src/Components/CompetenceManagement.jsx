import React, { useState } from 'react'
const CompetenceManagement = ({ competences, setCompetences }) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [level, setLevel] = useState(0)

  const handleCreateCompetence = () => {
    const newCompetence = { title, description, level }
    console.log(setCompetences)
    console.log(competences.length)
    console.log([newCompetence, ...competences])
    setCompetences([newCompetence, ...competences])
    setTitle('')
    setDescription('')
    setLevel(0)
  }

  const handleDeleteCompetence = index => {
    const updatedCompetences = competences.filter((_, i) => i !== index)
    setCompetences(updatedCompetences)
  }

  return (
    <div>
      <input
        type='text'
        placeholder='Заголовок'
        value={title}
        onChange={e => setTitle(e.target.value)}
      />
      <input
        type='text'
        placeholder='Описание'
        value={description}
        onChange={e => setDescription(e.target.value)}
      />
      <input
        type='number'
        placeholder='Уровень'
        value={level}
        onChange={e => setLevel(Number(e.target.value))}
      />
      <button onClick={handleCreateCompetence}>Создать</button>

      {competences.map((competence, index) => (
        <div key={index}>
          <h3>{competence.title}</h3>
          <p>{competence.description}</p>
          <p>Уровень: {competence.level}</p>
          <button onClick={() => handleDeleteCompetence(index)}>Удалить</button>
        </div>
      ))}
    </div>
  )
}

export default CompetenceManagement

import React from 'react'
const MyInformation = ({ myImage, myName, myDescription }) => {
  return (
    <>
      <div className='my-information'>
        <div className='my-photo'>
          <img src={myImage} alt='Моё фото' />
        </div>
        <div className='my-namedescription'>
          <h1>{myName}</h1>
          <p>{myDescription}</p>
        </div>
      </div>
    </>
  )
}

export default MyInformation

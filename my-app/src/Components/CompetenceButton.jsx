import React from 'react'

const CompetenceButton = ({ text, onClick, isToggled }) => {
  return (
    <button className='toggle-button' onClick={onClick}>
      {isToggled ? 'Скрыть' : 'Показать'} {text}
    </button>
  )
}

export default CompetenceButton
